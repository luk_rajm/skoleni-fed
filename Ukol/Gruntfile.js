module.exports = function(grunt) {
    var webpackConfig = require("./webpack.config.js");
    // Project configuration. 
    grunt.initConfig({
        sass: {                              // Task
            dist: {                            // Target
                // options: {                       // Target options
                //     style: 'compressed'
                // },
                files: [{                         // Dictionary of files
                    expand: true,
                    cwd: 'style/',
                    src: ['*.scss'],
                    dest: 'style/',
                    ext: '.css'       // 'destination': 'source'        
                }]
            }
        }, jshint: {
            all: ['Gruntfile.js', 'js/*.js', 'test/**/*.js'],
            options: {
                reporter: require('jshint-stylish')
            },
            target: ['file.js']
        }, watch: {
            css: {
                files: 'style/*.scss',
                tasks: ['sass']
            },
            scripts: {
                files: ['Gruntfile.js', 'js/*.js', 'test/**/*.js'],
                tasks: ['jshint'],
                options: {
                    spawn: false,
                },
            }
        },
        serve: {
            options: {
                port: 9000
            }
        },
        uncss: {
            dist: {
                files: {
                    'project/tidy.css': ['project/index.html']
                }
            }
        },
        webpack: {
            options: webpackConfig,

            stats: {
                // Configure the console output
                colors: true,
                modules: true,
                reasons: true
            },
            // stats: false disables the stats output

            // storeStatsTo: "xyz", // writes the status to a variable named xyz
            // // you may use it later in grunt i.e. <%= xyz.hash %>

            // progress: true, // Don't show progress
            // // Defaults to true

            // failOnError: true, // don't report error to grunt if webpack find errors
            // // Use this if webpack errors are tolerable and grunt should continue

            // watch: true, // use webpacks watcher
            // // You need to keep the grunt process alive

            // keepalive: true, // don't finish the grunt task
            // // Use this in combination with the watch option

            // inline: true,  // embed the webpack-dev-server runtime into the bundle
            // // Defaults to false

            // hot: true, // adds the HotModuleReplacementPlugin and switch the server to hot mode
            // // Use this in combination with the inline option       
        },
        jest: {
            options: {
                coverage: true,
                testPathPattern: /.*-test.js/
            }
        },
        "webpack-dev-server": {
            options: {
                webpack: webpackConfig
            },
            start: {
                keepAlive: true,
                webpack: {
                    devtool: "eval",
                    debug: true
                }
            }
        }
    });
    //
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-uncss');
    grunt.loadNpmTasks('grunt-serve');
    grunt.loadNpmTasks('grunt-webpack');
    grunt.loadNpmTasks('grunt-jest');


    // Default task(s).
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('mujsass', ['sass']);
    grunt.registerTask('hint', ['jshint']);
    grunt.registerTask('un', ['uncss']);
    grunt.registerTask('localhost', ['serve']);    
    grunt.registerTask('jestTest', ['jest']);
};