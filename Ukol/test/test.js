var webdriver = require('selenium-webdriver'),
    By = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until;

var driver = new webdriver.Builder()
    .forBrowser('firefox')
    .build();

driver.get('http://localhost:8080/index.html');
driver.findElement(By.id('valid')).sendKeys('webdriver');
driver.findElement(By.id('submit_button')).click();
driver.wait(until.titleIs(''), 1000);
driver.quit();