var AppDispatcher = require('../dispatcher/AppDispatcher');
var ItemConstants = require('../constants/ItemConstants');

var ItemActions = {
    createItem: function (item) {
        AppDispatcher.dispatch({
            actionType: ItemConstants.ITEM_CREATE,
            item: item
        });
    },
    updateItem: function (id,item) {
        AppDispatcher.dispatch({
            actionType: ItemConstants.ITEM_UPDATE,
            id:id,
            item:item
        });
    },
    
    itemCompleted: function (item) {
        var id = item.id;
        var actionType = item.completed ?
            ItemConstants.ITEM_UNCOMPLETED :
            ItemConstants.ITEM_COMPLETED;

        AppDispatcher.dispatch({
            actionType: actionType,
            id: id,
            item : item
        });
    },
    
    allCompleted: function () {
        AppDispatcher.dispatch({
            actionType: ItemConstants.ALL_COMPLETED
        });
    },

    deleteItem: function (id) {
        AppDispatcher.dispatch({
            actionType: ItemConstants.ITEM_DELETE,
            id: id
        });
    },

    deletedCompleted: function () {
        AppDispatcher.dispatch({
            actionType: ItemConstants.ITEM_DELETE_COMPLETED
        });
    }

};

module.exports = ItemActions;