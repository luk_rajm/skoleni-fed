import React from "react";
import ReactDOM from "react-dom";
import Header from "./Header.jsx";
import Main from "./MainSection.jsx";
import Footer from "./Footer.jsx";
import ItemStore from "../stores/ItemStore.js";



export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = { items: ItemStore.getAll(), areAllCompleted: ItemStore.areAllCompleted() };
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        ItemStore.addChangeListener(this.onChange);
    }

    componentWillUnmount() {
        ItemStore.removeChangeListener(this.onChange);
    }

    onChange() {
        this.setState({ items: ItemStore.getAll(), areAllCompleted: ItemStore.areAllCompleted() });
    }

    render() {
        return <div>
            <Header className="header"/>
            <Main className="main"
                allItems = {this.state.items}
                areAllCompleted ={this.state.areAllCompleted} />
            <Footer allItems = {this.state.items} />
        </div>;
    }

}

ReactDOM.render(<App />, document.getElementById('app'));


