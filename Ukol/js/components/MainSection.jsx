import React from "react";
import Item from "./Item.jsx";
import ItemActions from "../actions/ItemActions";
import {Checkbox} from "react-bootstrap";

export default class MainSection extends React.Component {
    constructor(props) {
        super(props);
        this.onAllCompleted = this.onAllCompleted.bind(this);
    }

    onAllCompleted() {
        ItemActions.allCompleted();
    }

    render() {

        if (Object.keys(this.props.allItems).length < 1) {
            return null;
        }

        var allItems = this.props.allItems;
        var items = [];

        for (var key in allItems) {
            items.push(<Item key={key} item={allItems[key]} />);
        }

        return <section>
            <div className="list-group-item">
                <div className="list-gourp row">
                    <div className="list-gourp-item col-xs-2">
                        <Checkbox  
                         onChange={this.onAllCompleted} 
                         checked={this.props.areAllCompleted ? true : false}
                        />
                    </div>
                    <div className="list-gourp-item col-xs-10"><h2>Seznam položek</h2></div>
                </div>
            </div>
            <ul className="list-group">{items}</ul>
        </section>;
    }
}
