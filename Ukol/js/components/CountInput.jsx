import React from "react";
import {FormControl} from "react-bootstrap";

export default class CountInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = { count: this.props.value || 1 };
        this.onChangeCount = this.onChangeCount.bind(this);
        this.save = this.save.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
    }

    onChangeCount(e) {
        this.setState({ count: e.target.value })
    }

    clearInput() {
        this.setState({ count: 1 });
    }

    onKeyDown(e) {
        if (e.keyCode === 13) {            
                this.save();
        }
    }

    save() {
        if (this.props.onSave) {
            this.props.onSave(this.state.count);
            this.clearInput();
        }
    }

    render() {
        return <div>
            <FormControl
                type="number"
                min="1"
                value={this.state.count}
                onChange ={this.onChangeCount}
                onBlur={this.save}
                onKeyDown={this.onKeyDown}
                />
        </div>;
    }
}