import React from "react";
import {Button, Checkbox, ControlLabel} from "react-bootstrap";
import ItemActions from "../actions/ItemActions";
import NameInput from "./NameInput.jsx";
import CountInput from "./CountInput.jsx";

export default class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = { editNameMode: false, editCountMode: false, validationText: "" }
        this.onDeleteClick = this.onDeleteClick.bind(this);
        this.onDoubleClickName = this.onDoubleClickName.bind(this);
        this.onDoubleClickCount = this.onDoubleClickCount.bind(this);
        this.onSaveName = this.onSaveName.bind(this);
        this.onSaveCount = this.onSaveCount.bind(this);
        this.onItemCompleted = this.onItemCompleted.bind(this);
        this.setValidationResults = this.setValidationResults.bind(this);
    }

    onDeleteClick() {
        ItemActions.deleteItem(this.props.item.id);
    }

    onDoubleClickName() {
        if(!this.props.item.completed)
            this.setState({ editNameMode: true, editCountMode: false });
    }

    onDoubleClickCount() {
        if(!this.props.item.completed)
            this.setState({ editCountMode: true, editNameMode: false });                
    }

    onSaveCount(count) {
        let item = this.props.item;
        item.count = count
        ItemActions.updateItem(item.id, item)
        this.setState({ editCountMode: false });
    }

    onSaveName(name) {
        let item = this.props.item;
        item.name = name
        ItemActions.updateItem(item.id, item)
        this.setState({ editNameMode: false, validationText: "" });
    }

    onItemCompleted() {
        ItemActions.itemCompleted(this.props.item);
    }

    setValidationResults(validation) {
        this.setState({ validationText: "" });
        validation.forEach(function (v) {
            if (!v.isValid)
                this.setState({ validationText: v.validationText });
        }, this);
    }    
    

    render() {
        let item = this.props.item;

        let input;
        if (this.state.editNameMode && !item.completed) {
            input = <div className="list-gourp-item  col-xs-offset-2 col-xs-5 ">
                <NameInput setValidationResults = {this.setValidationResults} onSave={this.onSaveName} value={item.name}/>
                <p id="invalid">{this.state.validationText}</p>
            </div>
        }
        if (this.state.editCountMode && !item.completed) {
            input = <div className="list-gourp-item  col-xs-offset-7 col-xs-3"> <CountInput onSave={this.onSaveCount} value={item.count}/> </div>
        }

        return <li
            className="list-group-item"
            key={item.id}>
            <div className="list-gourp row">
                <div className="list-gourp-item col-xs-2">
                    <Checkbox
                        checked={item.completed}
                        onChange={this.onItemCompleted}
                        />
                </div>
                <div className="list-gourp-item col-xs-5">
                    <ControlLabel className={item.completed ? "complete":""} title={item.name} onClick={this.onDoubleClickName}> {item.name}</ControlLabel>
                </div>
                <div className="list-gourp-item col-xs-3">
                    <ControlLabel className="badge" onClick={this.onDoubleClickCount}>{item.count}</ControlLabel>
                </div>
                <div className="list-gourp-item col-xs-2">
                    <Button className="delete" onClick={this.onDeleteClick}>X</Button>
                </div>
            </div>
            <div className="list-gourp row">
                {input}
            </div>
        </li>;
    }
}