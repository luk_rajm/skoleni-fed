import React from "react";
import ReactDOM from "react-dom";
import NameInput from "./NameInput.jsx";
import CountInput from "./CountInput.jsx";
import {Button, Form, FormGroup, ControlLabel, FormControl} from "react-bootstrap";

export default class AddForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { validationText: '', disabledButton: true };
        this.onChangeCount = this.onChangeCount.bind(this);
        this.setValidationResults = this.setValidationResults.bind(this);
        this.setValidationFormElements = this.setValidationFormElements.bind(this);
        this.resetValidationFormElements = this.resetValidationFormElements.bind(this);
        this.onSubmit = this.onSubmit.bind(this);     
    }

    setValidationFormElements(disabled, text) {
        this.setState({ validationText: text, disabledButton: disabled });
    }

    resetValidationFormElements() {
        this.setValidationFormElements(false, '');
    }

    setValidationResults(validation) {
        this.resetValidationFormElements();
        validation.forEach(function (v) {
            if (!v.isValid)
                this.setValidationFormElements(true, v.validationText);
        }, this);
    }


    onChangeCount(e) {
        var currentValue = e.target.value;
        this.setState({ count: currentValue })
    }    
    
    onSubmit(e) {
        e.preventDefault();
        let nameValue = this.refs.nameInput.state.name;
        let countValue = this.refs.countInput.state.count;
        this.refs.nameInput.clearInput();
        this.refs.countInput.clearInput();
        this.props.onSave({ name: nameValue, count: countValue })
        this.setState({disabledButton: true })
    }

    render() {
        return <div>
            <Form id="add_form" className="form-horizontal" onSubmit={this.onSubmit}>
                <FormGroup>
                    <div className="col-sm-8">
                        <ControlLabel>Název: </ControlLabel>
                        <NameInput setValidationResults = {this.setValidationResults} ref="nameInput" />
                    </div>
                    <div className="col-sm-2">
                        <ControlLabel>Množství: </ControlLabel>
                        <CountInput ref="countInput" />
                    </div>
                    <div className="col-sm-2">
                        <Button type = "submit" id="submit_button" bsSize="small" disabled = {this.state.disabledButton} >Pridej</Button>
                    </div>
                </FormGroup>
                <p id="invalid">{this.state.validationText}</p>
            </Form>
        </div>;
    }
}


