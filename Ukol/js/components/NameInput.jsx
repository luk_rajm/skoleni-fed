import React from "react";
import {FormControl} from "react-bootstrap";

export default class NameInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isValid: true, name:  this.props.value || "" };
        this.onChangeName = this.onChangeName.bind(this);
        this.validateNameProduct = this.validateNameProduct.bind(this);
        this.save = this.save.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
    }

    validateNameProduct(inputValue) {
        let pattern = new RegExp("^[A-zÀ-ÿÁ-ž ]+$"); //^[A-zÀ-ÿÁ-ž ]+$  - obsahu je jen slova
        let isText = pattern.test(inputValue);
        let isNotEmpty = inputValue.length > 0;
        let isValid = isText && isNotEmpty;
        return {
            valid: isValid,
            validation: [{
                isValid: isText,
                validationText: "Název produktu nesmí obsahovat specialni znaky a čisla!"
            },
                {
                    isValid: isNotEmpty,
                    validationText: "Název produktu nesmí být prázdný!"
                }]
        };
    }

    onChangeName(e) {
        let currentValue = e.target.value;
        let validationResult = this.validateNameProduct(currentValue);
        this.setState({ isValid: validationResult.valid, name: currentValue })
        if (this.props.setValidationResults) {
            this.props.setValidationResults(validationResult.validation);
        }
    }

    clearInput() {
        this.setState({ name: "" });
    }

    onKeyDown(e) {
        if (e.keyCode === 13) {
            if(this.props.onSave && this.state.isValid)
                this.save();                        
        }
    }
    
    save() {        
        if (this.props.onSave && this.state.isValid ) {
            this.props.onSave(this.state.name);
            this.clearInput();
        }
    }


    render() {
        const invalidClassName = "invalid";
        const validClassName = "valid";
        var className = this.state.isValid ? validClassName : invalidClassName;
        return <div>
            <FormControl
                type="text"
                placeholder="Název položky"
                id={className}                
                value={this.state.name}
                onChange ={this.onChangeName}
                onBlur={this.save}
                onKeyDown={this.onKeyDown}
                /></div>;
    }
}