import React from "react";
import AddForm from "./AddForm.jsx";
import ItemActions from "../actions/ItemActions";

export default class Header extends React.Component {
    constructor(props) {
        super(props);             
    }
    
    onSave(item) {    
      ItemActions.createItem(item);
    }  
    
    render() {
        return <header>
            <h1>Nákupní seznam</h1>
            <AddForm  onSave = {this.onSave}/>
        </header>;
    }
}

