import React from "react";
import ItemActions from "../actions/ItemActions";
import {Button, ControlLabel} from "react-bootstrap";


export default class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.state = { items: this.props.allItems };
         this.onClick = this.onClick.bind(this);
    }
        
    onClick() {
        ItemActions.deletedCompleted();
    }


    render() {
        let items = [];
        for (var id in this.props.allItems) {
            items.push(this.props.allItems[id]);
        }
        
        if (items.length < 1) {
            return null;
        }

        let hideButton = false;
        let countLeft = 0;
        if (items.length > 0) {
            countLeft = items.filter(v => !v.completed).length;
            hideButton = items.filter(v => v.completed).length > 0;
        }

        let button = hideButton ? <Button className="delete-completed" onClick={this.onClick}>Smazat kompletní</Button> : null;

        return <footer>
            <div className="panel panel-default">
                <div className="panel-body">
                    <ControlLabel>Zbývá ({countLeft}) </ControlLabel>
                    {button}
                </div>
            </div>
        </footer>
    }
}
