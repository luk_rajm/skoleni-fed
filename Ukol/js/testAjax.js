var token;

function loadUuToken() {
    $.ajax({
        url: "https://uuos9.plus4u.net/uu%3A0%3AARTIFACT%3FUUproto%3AOS%2FService%2FLogin%3AcallTokenGenerate",
        type: 'POST',
        headers: {
            "content-type": "application/json",
            "uu_eac": "6861666861663a6e61666e6166",
            "cache-control": "no-cache",
            "postman-token": "93484b1b-a212-1473-42bb-f27a13f00be6"
        },
        data: "{\"data\":{\"outputTo\":\"data\"}}"
    }).then( function (msg){
        console.log(msg);
        var data  = JSON.parse(msg);
            token = data.data.UU_CT;
        console.log(token);
        /*loadList();*/
        saveValue();        
    });
}

function loadList() {
    $.ajax({
        url: "https://uuos9.plus4u.net/uu%3ADEV0135-BT%3AARTIFACT%3FUU%3ASTORAGE%3AStorage%2Flist",
        type: 'POST',
        headers: {
            "accept": "application/json",
            "uu_ct": token
        },
        data: ""
    }).then( function (msg){
        console.log(msg);
    });
}

function saveValue() {
    $.ajax({
        url: "https://uuos9.plus4u.net/uu%3ADEV0135-BT%3AARTIFACT%3FUU%3ASTORAGE%3AStorage%2Fsave",
        type: 'POST',
        headers: {
            "accept": "application/json",
            "uu_ct": token
        },
        data: JSON.stringify({data: {dataKey : "LR", parameter : "rohlik"} })
    }).then( function (msg){
        console.log(msg);
        loadValue(); 
    });
}

function loadValue() {
    $.ajax({
        url: "https://uuos9.plus4u.net/uu%3ADEV0135-BT%3AARTIFACT%3FUU%3ASTORAGE%3AStorage%2Fload",
        type: 'POST',
        headers: {
            "accept": "application/json",
            "uu_ct": token
        },
        data: JSON.stringify({data: {dataKey : "LR"} })
    }).then( function (msg){
        console.log(msg);
        deleteValue();
    });
}

function deleteValue() {
    $.ajax({
        url: "https://uuos9.plus4u.net/uu%3ADEV0135-BT%3AARTIFACT%3FUU%3ASTORAGE%3AStorage%2Fdelete",
        type: 'POST',
        headers: {
            "accept": "application/json",
            "uu_ct": token
        },
        data: JSON.stringify({data: {dataKey : "LR"} })
    }).then( function (msg){
        console.log(msg);
    });
}