var AppDispatcher = require('../dispatcher/AppDispatcher');
var ItemConstants = require('../constants/ItemConstants');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var CHANGE_EVENT = 'change';

var items = {};

function createItem(item) {
  var id = (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
  items[id] = {
    id: id,
    name: item.name,
    count: item.count,
    completed: false
  };
}

function updateItem(id, updates) {
  items[id] = assign({}, updates);
}

function updateAllItems(updates) {
  for (var id in items) {
    var item = items[id];
    item.completed = updates;
    updateItem(id, item);
  }
}

function deleteItem(id) {
  delete items[id];
}

function deleteCompletedItems() {
  for (var id in items) {
    if (items[id].completed) {
      deleteItem(id);
    }
  }
}

var ItemStore = assign({}, EventEmitter.prototype, {

  areAllCompleted: function () {
    for (var id in items) {
      if (!items[id].completed) {
        return false;
      }
    }
    return true;
  },

  getAll: function () {
    return items;
  },

  emitChange: function () {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function (callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function (callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

AppDispatcher.register(function (action) {
  var text;

  switch (action.actionType) {
    case ItemConstants.ITEM_CREATE:
      createItem(action.item);
      ItemStore.emitChange();
      break;

    case ItemConstants.ALL_COMPLETED:
      if (ItemStore.areAllCompleted()) {
        updateAllItems(false);
      } else {
        updateAllItems(true);
      }
      ItemStore.emitChange();
      break;

    case ItemConstants.ITEM_UNCOMPLETED:
      action.item.completed = false;
      updateItem(action.id, action.item);
      ItemStore.emitChange();
      break;

    case ItemConstants.ITEM_COMPLETED:
      action.item.completed = true;
      updateItem(action.id, action.item);
      ItemStore.emitChange();
      break;

    case ItemConstants.ITEM_UPDATE:
      updateItem(action.id, action.item);
      ItemStore.emitChange();
      break;

    case ItemConstants.ITEM_DELETE:
      deleteItem(action.id);
      ItemStore.emitChange();
      break;

    case ItemConstants.ITEM_DELETE_COMPLETED:
      deleteCompletedItems();
      ItemStore.emitChange();
      break;

    default:
    // no op
  }
});

module.exports = ItemStore;
