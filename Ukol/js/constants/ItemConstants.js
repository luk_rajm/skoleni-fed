module.exports =  Object.assign({
  ITEM_CREATE: "item_create",
  ITEM_DELETE: "item_delete",
  ITEM_UPDATE: "item_update",
  ITEM_DELETE_COMPLETED: "delete_comleted",
  ITEM_COMPLETED: "item_completed",
  ITEM_UNCOMPLETED: "item_uncompleted",
  ALL_COMPLETED: "all_completed"
});