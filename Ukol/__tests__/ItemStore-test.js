jest.dontMock('../js/stores/ItemStore');
jest.dontMock('../js/constants/ItemConstants');
jest.dontMock('object-assign');

describe('ItemStore', function () {

    var ItemConstants = require('../js/constants/ItemConstants');
    var AppDispatcher;
    var ItemStore;
    var callback;

    // mock actions
    var actionCreateItem = {
        actionType: ItemConstants.ITEM_CREATE,
        item: { name: "Rohliky", count: 11 }
    };
    var actionDeleteItem = {
        actionType: ItemConstants.ITEM_DELETE,
        id: 'replace me in test'
    };

    beforeEach(function () {
        AppDispatcher = require('../js/dispatcher/AppDispatcher');
        ItemStore = require('../js/stores/ItemStore');
        callback = AppDispatcher.register.mock.calls[0][0];
    });

    it('registers a callback with the dispatcher', function () {
        expect(AppDispatcher.register.mock.calls.length).toBe(1);
    });

    it('should initialize with no items', function () {
        var all = ItemStore.getAll();
        expect(all).toEqual({});
    });

    it('creates item', function () {
        callback(actionCreateItem);
        var all = ItemStore.getAll();
        var keys = Object.keys(all);
        expect(keys.length).toBe(1);
        expect(all[keys[0]].name).toEqual('Rohliky');
        expect(all[keys[0]].count).toEqual(11);
        expect(all[keys[0]].completed).toEqual(false);
    });

    it('delete item', function () {
        callback(actionCreateItem);
        var all = ItemStore.getAll();
        var keys = Object.keys(all);
        expect(keys.length).toBe(1);
        actionDeleteItem.id = keys[0];
        callback(actionDeleteItem);
        expect(all[keys[0]]).toBeUndefined();
    });

    it('update item', function () {
        var actionUpdateItem = {
            actionType: ItemConstants.ITEM_UPDATE,
            id: 'replace me in test',
            item: { name: "Zemle", count: 11, completed :true }
        };
        callback(actionCreateItem);
        var all = ItemStore.getAll();
        var keys = Object.keys(all);
        expect(keys.length).toBe(1);
        actionUpdateItem.id = keys[0];
        callback(actionUpdateItem);
        all = ItemStore.getAll();
        keys = Object.keys(all);
        expect(all[keys[0]].name).toEqual('Zemle');
        expect(all[keys[0]].count).toEqual(11);
        expect(all[keys[0]].completed).toEqual(true);
    });
});
