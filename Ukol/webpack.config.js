var ExtractTextPlugin = require("extract-text-webpack-plugin");
var extractCSS = new ExtractTextPlugin('bundle.css');
module.exports = {
    // context: __dirname + "/js/components",
    entry:  "./js/components/App.jsx" ,
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    // devtool: 'source-map',
    module: {
        loaders: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                loader: "babel",
                query: {
                    presets: ["es2015", "react"]
                }
            },
            {
                test: /\.scss$/,
                include: /style/,                
                loaders: [{
                    test: /\.scss$/, loader: extractCSS.extract("style", "css!sass?sourceMap")
                }]
            }
        ]
    },
    plugins: [
        extractCSS
    ]
};